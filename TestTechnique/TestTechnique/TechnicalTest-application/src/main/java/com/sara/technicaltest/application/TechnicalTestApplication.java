package com.sara.technicaltest.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.sara.technicaltest.web.configuration.TechnicalTestWebConfiguration;
/**
 * application entry point
 */
@SpringBootApplication
@Import(TechnicalTestWebConfiguration.class)
public class TechnicalTestApplication {

	/**
	 * Main entry point for starting the Technical Test application.
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(TechnicalTestApplication.class, args);
	}

}
