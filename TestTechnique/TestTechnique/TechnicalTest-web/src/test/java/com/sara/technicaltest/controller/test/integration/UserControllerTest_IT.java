package com.sara.technicaltest.controller.test.integration;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.sara.technicaltest.business.dto.UserDto;
import com.sara.technicaltest.business.mapper.UserMapper;
import com.sara.technicaltest.business.service.UserService;
import com.sara.technicaltest.data.entity.UserEntity;
import com.sara.technicaltest.web.controller.UserController;


@AutoConfigureMockMvc
@ContextConfiguration(classes = {UserController.class})
@WebMvcTest
@DisplayName("Testing WS /users via the http layer")
@Tag("Integration")
public class UserControllerTest_IT {

    public static final String BASE_URL = "users";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private UserMapper userMapper;

    private UserDto userDto;

    private static final String JSON_USER_VALUE = "{\"firstName\":\"Sara\",\"lastName\":\"TALEB\",\"email\":\"sara.taleb@alithya.com\",\"birthdate\":\"2000-12-31\",\"phoneNumber\":\"0711223344\",\"gender\":\"Female\",\"countryOfResidence\":\"France\"}";
    private static final String BASE_URL_GET = "/users";
    @BeforeEach
    void init() {
        final Long userId = 1L;
        final LocalDate birthdate = LocalDate.of(2000, 12, 31);
        userDto = UserDto.builder()
                .idUser(userId)
                .firstName("Sara")
                .lastName("TALEB")
                .email("sara.taleb@alithya.com")
                .birthdate(birthdate)
                .phoneNumber("0711223344")
                .gender("Female")
                .countryOfResidence("France")
                .build();
    }

    @Test
    @DisplayName("Test the WS to add a user. POST url: /users")
    void create() throws Exception {

        when(userService.addUser(Mockito.any(UserDto.class))).thenReturn(userDto);

        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        
        mockMvc.perform(MockMvcRequestBuilders.post("/" + BASE_URL)
                .content(objectMapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(content().json(JSON_USER_VALUE))
                .andReturn();
    }
    
    
    @Test
    @DisplayName("Test the WS to get a user by ID. GET url: /users/{userId}")
    void getUserById() throws Exception {
        final Long userId = 1L;

        when(userService.getUserById(userId)).thenReturn(userDto);

        mockMvc.perform(MockMvcRequestBuilders.get(BASE_URL_GET + "/" + userId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idUser").value(userId))
                .andExpect(jsonPath("$.firstName").value(userDto.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(userDto.getLastName()))
                .andExpect(jsonPath("$.email").value(userDto.getEmail()))
                .andExpect(jsonPath("$.birthdate").value(userDto.getBirthdate().toString()))
                .andExpect(jsonPath("$.phoneNumber").value(userDto.getPhoneNumber()))
                .andExpect(jsonPath("$.gender").value(userDto.getGender()))
                .andExpect(jsonPath("$.countryOfResidence").value(userDto.getCountryOfResidence()));
    }
    

}
