package com.sara.technicaltest.web.configuration;

import lombok.Builder;
import lombok.Getter;

/**
 * Error message.
 */
@Getter
@Builder
public class Error {

	private String message;
}
