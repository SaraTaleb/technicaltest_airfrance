package com.sara.technicaltest.web.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.sara.technicaltest.business.configuration.TechnicalTestBusinessConfiguration;
import com.sara.technicaltest.business.service.UserService;
import com.sara.technicaltest.web.controller.UserController;

/**
 * Configuration class for the web layer
 */
@Configuration(proxyBeanMethods = false)
@Import({
	OpenApiConfiguration.class,
	TechnicalTestBusinessConfiguration.class,
	GlobalExceptionHandler.class
})
public class TechnicalTestWebConfiguration {

	@Bean
	UserController userController(UserService userService) {
		return new UserController(userService);
	}

}
