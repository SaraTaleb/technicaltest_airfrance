package com.sara.technicaltest.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sara.technicaltest.business.configuration.LogExecutionTime;
import com.sara.technicaltest.business.dto.UserDto;
import com.sara.technicaltest.business.service.UserService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Rest call management controller for user entity
 */
@RestController
@RequestMapping("/users")
@Slf4j
@RequiredArgsConstructor
public class UserController {

	private final UserService userService;
	
	/**
	 * find a user by user ID
	 * 
	 * @param userId user ID
	 * @return {@link UserDto}
	 */
	@Operation(summary = "Get a user by ID")
	@ApiResponse(responseCode = "200", description = "Found user")
	@ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
	@ApiResponse(responseCode = "404", description = "Not found", content = @Content)
	@ApiResponse(responseCode = "500", description = "Internal error", content = @Content)
	@GetMapping(path = "/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@LogExecutionTime
	public ResponseEntity<UserDto> getUser(@PathVariable("userId") final Long userId) {
		log.info("Find the user with ID : {}" , userId);
		UserDto userDto = userService.getUserById(userId);
		if (userDto != null) {
			return ResponseEntity.ok(userDto);
		}
		return ResponseEntity.notFound().build();
	}

	/**
	 * Create and return a user
	 * @param userDto the user to add
	 * @return {@link UserDto}
	 */
	@Operation(summary = "Save a user")
	@ApiResponse(responseCode = "201", description = "user saved")
	@ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
	@ApiResponse(responseCode = "404", description = "Not found", content = @Content)
	@ApiResponse(responseCode = "500", description = "Internal error", content = @Content)

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@LogExecutionTime
	public ResponseEntity<UserDto> addUser(@Valid @RequestBody UserDto userDto) {

		log.info("Create the user = {}" , userDto);
		UserDto savedUserDto = userService.addUser(userDto);
		return ResponseEntity.status(HttpStatus.CREATED).body(savedUserDto);
	}
}
