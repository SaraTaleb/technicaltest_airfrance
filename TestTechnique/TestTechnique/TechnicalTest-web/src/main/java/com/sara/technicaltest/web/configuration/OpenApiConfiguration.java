package com.sara.technicaltest.web.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;

/**
 * Configuration class for OpenAPI documentation generation.
 */
@Configuration(proxyBeanMethods = false)
public class OpenApiConfiguration {

	@Bean
	OpenAPI customOpenAPI() {
		return new OpenAPI().info(apiInfo());	
	}

	private Info apiInfo() {
		return new Info()
			.title("Documentation du test 'Air France' ")
			.description("Documentation de l'api REST pour le test 'Air France' ")
			.contact(apiContact());
	}

	private Contact apiContact() {
		return new Contact()
			.name("Sara TALEB")
			.email("sara.taleb@alithya.com");
	}
}
