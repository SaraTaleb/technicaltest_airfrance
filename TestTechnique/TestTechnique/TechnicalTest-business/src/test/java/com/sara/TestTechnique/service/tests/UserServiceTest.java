package com.sara.TestTechnique.service.tests;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoOperations;

import com.sara.technicaltest.business.dto.UserDto;
import com.sara.technicaltest.business.exception.UserAlreadyExistsException;
import com.sara.technicaltest.business.mapper.UserMapper;
import com.sara.technicaltest.business.service.impl.UserServiceImpl;
import com.sara.technicaltest.data.entity.MongoUtil;
import com.sara.technicaltest.data.entity.UserEntity;
import com.sara.technicaltest.data.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
@DisplayName("User management service unit tests")
@Tag("Unitaires")
public class UserServiceTest {

	@InjectMocks
	private UserServiceImpl userServiceImpl;

	@Mock
	private UserMapper userMapper;
	@Mock
	private UserRepository userRepository;


	@Test
	@DisplayName("Testing a service that retrieves a user")
	public void getUser() {

		final Long userId = 1L;
		final LocalDate birthdateTime = LocalDate.of(2000, 12, 31);

		UserEntity userEntity = new UserEntity(userId, "Sara", "TALEB", "sara.taleb@alithya.com", birthdateTime,
				"0711223344", "Female", "France");

		UserDto userDto = new UserDto(userId, "Sara", "TALEB", "sara.taleb@alithya.com", birthdateTime, "0711223344",
				"Female", "France");
		when(userRepository.findById(userId)).thenReturn(Optional.of(userEntity));
		when(userMapper.entityToDto(userEntity)).thenReturn(userDto);
		UserDto resultat = userServiceImpl.getUserById(userId);

		assertNotNull(resultat);
		assertNotNull(resultat.getIdUser());
		assertEquals(userId, resultat.getIdUser());

		verify(userRepository).findById(userId);
		verify(userMapper).entityToDto(userEntity);
	}


	@Test
	@DisplayName("Test a service that checks the exception for an existing user")
	public void saveUser_UserAlreadyExists() {
		final Long userId = 1L;
		final LocalDate birthdateTime = LocalDate.of(2000, 12, 31);

		UserDto userDto = new UserDto(userId, "Sara", "taleb", "sara.taleb@alithya.com", birthdateTime, "0711223344",
				"Female", "France");
		UserEntity userEntity = UserEntity.builder().idUser(userId).firstName("Sara").lastName("taleb")
				.email("sara.taleb@alithya.com").birthdate(birthdateTime).phoneNumber("0711223344").gender("Female")
				.countryOfResidence("France").build();

		when(userMapper.dtoToEntity(userDto)).thenReturn(userEntity);
		when(userRepository.findByEmail(userDto.getEmail())).thenReturn(Optional.of(userEntity));

		try {
			userServiceImpl.addUser(userDto);
			fail("Expected UserAlreadyExistsException to be thrown");
		} catch (UserAlreadyExistsException e) {
		}

		verify(userMapper).dtoToEntity(userDto);
		verify(userRepository).findByEmail(userDto.getEmail());
		verify(userRepository, never()).save(any(UserEntity.class));
	}

}
