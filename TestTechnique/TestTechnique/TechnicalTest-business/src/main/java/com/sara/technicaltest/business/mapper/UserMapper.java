package com.sara.technicaltest.business.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.sara.technicaltest.business.dto.UserDto;
import com.sara.technicaltest.data.entity.UserEntity;

/**
 * Mapping dto to entity and entity to dto
 */
@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
    /**
     * Converts a {@link UserEntity} object to a {@link UserDto} object.
     *
     * @param userEntity the {@link UserEntity} object to convert
     * @return {@link UserDto}
     */
    @Mappings({
//        @Mapping(target = "idUser", source = "idUser"),
        @Mapping(target = "firstName", source = "firstName"),
        @Mapping(target = "lastName", source = "lastName"),
        @Mapping(target = "email", source = "email"),
        @Mapping(target = "birthdate", source = "birthdate"),
        @Mapping(target = "phoneNumber", source = "phoneNumber"),
        @Mapping(target = "gender", source = "gender"),
        @Mapping(target = "countryOfResidence", source = "countryOfResidence")
    })
    UserDto entityToDto(UserEntity userEntity);

    /**
     * Converts a {@link UserDto} object to a {@link UserEntity} object.
     *
     * @param userDto the {@link UserDto} object to convert
     * @return {@link UserEntity}
     */
    @Mappings({
//        @Mapping(target = "idUser", source = "idUser"),
        @Mapping(target = "firstName", source = "firstName"),
        @Mapping(target = "lastName", source = "lastName"),
        @Mapping(target = "email", source = "email"),
        @Mapping(target = "birthdate", source = "birthdate"),
        @Mapping(target = "phoneNumber", source = "phoneNumber"),
        @Mapping(target = "gender", source = "gender"),
        @Mapping(target = "countryOfResidence", source = "countryOfResidence")
    })
    UserEntity dtoToEntity(UserDto userDto);
}