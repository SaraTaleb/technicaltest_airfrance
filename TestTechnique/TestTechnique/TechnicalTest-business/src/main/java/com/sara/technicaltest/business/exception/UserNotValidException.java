package com.sara.technicaltest.business.exception;

/**
 * Exception thrown when a user is found to be invalid based on certain criteria.
 * Extends {@link IllegalArgumentException}.
 */
public class UserNotValidException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;

	public UserNotValidException(String message) {
		super(message);
	}
	
}
