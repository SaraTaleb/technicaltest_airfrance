package com.sara.technicaltest.business.configuration;

import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.sara.technicaltest.business.mapper.UserMapper;
import com.sara.technicaltest.business.service.impl.UserServiceImpl;
import com.sara.technicaltest.data.configuration.TechnicalTestDataConfiguration;
import com.sara.technicaltest.data.entity.MongoUtil;
import com.sara.technicaltest.data.repository.UserRepository;

/**
 * Configuration class for the business layer
 */
@Configuration(proxyBeanMethods = false)
@Import({
	LoggingAspect.class, 
	TechnicalTestDataConfiguration.class
})
public class TechnicalTestBusinessConfiguration {

	@Bean
	UserServiceImpl userService(UserRepository userRepository, MongoUtil mongoUtil) {
		return new UserServiceImpl(userRepository, Mappers.getMapper(UserMapper.class), mongoUtil);
	}

}
