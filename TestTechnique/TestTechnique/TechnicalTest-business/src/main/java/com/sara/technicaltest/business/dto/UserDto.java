package com.sara.technicaltest.business.dto;

import java.time.LocalDate;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * User dto
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class UserDto {

    private Long idUser;

    @NotBlank(message = "firstname is mandatory")
    @Size(min = 1, max = 64, message = "firstName must be between {min} and {max} characters long")
    private String firstName;

    @NotBlank(message = "lastName is mandatory")
    @Size(min = 1, max = 64, message = "lastName must be between {min} and {max} characters long")
    private String lastName;

    @Email(message = "Invalid email address")
    @NotBlank(message = "email adress is mandatory")
    @Size(min = 1, max = 64, message = "Username must be between {min} and {max} characters long")
    private String email;

    @Past(message = "Birth date must be in the past")
    @NotNull(message = "birthdate is mandatory")
    private LocalDate birthdate;

    //Optional field
    private String phoneNumber;

    //Optional field
    private String gender;

    @NotBlank(message = "country Of Residence adress is mandatory")
    private String countryOfResidence;
}
