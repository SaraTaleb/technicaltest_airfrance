package com.sara.technicaltest.business.exception;

/**
 * Exception thrown when attempting to create a user that already exists.
 * Extends {@link IllegalArgumentException}.
 */
public class UserAlreadyExistsException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;

	public UserAlreadyExistsException(String message) {
		super(message);
	}
	
}