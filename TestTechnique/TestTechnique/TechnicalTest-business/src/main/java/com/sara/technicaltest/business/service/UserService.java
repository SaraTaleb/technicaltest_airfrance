package com.sara.technicaltest.business.service;

import com.sara.technicaltest.business.dto.UserDto;

/**
 * User service
 */
public interface UserService {

	/**
	 * Returns a user according to its identifier
	 * @param userId the user's identifier
	 * @return { {@link UserDto}}
	 */
	UserDto getUserById(Long userId);
	
	/**
	 * Create and return a user
	 * @param user the user to save
	 * @return {@link UserDto}
	 */
	UserDto addUser(UserDto user);
}
