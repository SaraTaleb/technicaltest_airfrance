package com.sara.technicaltest.business.service.impl;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.util.Assert;

import com.sara.technicaltest.business.configuration.LogExecutionTime;
import com.sara.technicaltest.business.dto.UserDto;
import com.sara.technicaltest.business.exception.UserAlreadyExistsException;
import com.sara.technicaltest.business.exception.UserNotValidException;
import com.sara.technicaltest.business.mapper.UserMapper;
import com.sara.technicaltest.business.service.UserService;
import com.sara.technicaltest.data.entity.MongoUtil;
import com.sara.technicaltest.data.entity.UserEntity;
import com.sara.technicaltest.data.repository.UserRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 * User service implementation
 */
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;
	
	private final UserMapper userMapper;
	
	private final MongoUtil mongoUtil;
	
	@Override
	@LogExecutionTime
	public UserDto getUserById(Long userId) {
		
		Assert.notNull(userId, "UserId must not be null");
		
		Optional<UserEntity> userEntityOptional = userRepository.findById(userId);
		
		if (!userEntityOptional.isPresent()) {
			log.warn("User not found with id {}", userId);
			return null;
		}
		return userMapper.entityToDto(userEntityOptional.get());
	}

	@Override
	@LogExecutionTime
	public UserDto addUser(UserDto userDto) {

		Assert.notNull(userDto, "User must not be null");
		
		UserEntity userEntity = userMapper.dtoToEntity(userDto);
		
		if (!isValidUser(userEntity)) { 
			throw new UserNotValidException("User is either not over 18 years old or not from France. He's not registered : "+ userDto);
		}
		
		 Optional<UserEntity> userInDataBase = userRepository.findByEmail(userDto.getEmail());
		 if(userInDataBase.isPresent()) {
				throw new UserAlreadyExistsException("User already existes with Email: "+ userDto.getEmail());
		 }
		
		userEntity.setIdUser(mongoUtil.generateSequence(UserEntity.SEQUENCE_NAME));
		userRepository.save(userEntity);
		log.info("Saved user with ID {}", userEntity.getIdUser());

		return getUserById(userEntity.getIdUser());
	}
	
	/**
	 * This method allows you to determine whether the user meets the following criteria: 
	 * he or she is over 18 and lives in France
	 * @param user the user to check
	 * @return {@link Boolean}
	 */
	private boolean isValidUser(UserEntity user) {
		
		LocalDate now = LocalDate.now();
		LocalDate eighteenYearsAgo = now.minusYears(18);
		LocalDate birthDate = user.getBirthdate();
		
		boolean isOverEighteen = birthDate != null  && birthDate.isBefore(eighteenYearsAgo);
		boolean isFromFrance = "France".equalsIgnoreCase(user.getCountryOfResidence());
		
		return isOverEighteen && isFromFrance;
	}
}
