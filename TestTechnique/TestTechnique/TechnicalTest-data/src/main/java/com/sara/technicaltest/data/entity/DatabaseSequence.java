package com.sara.technicaltest.data.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents a database sequence document used for generating unique sequence
 * numbers.
 */

@Getter
@Setter
@Document(collection = "userdatabase_sequences")
public class DatabaseSequence {

	/**
	 * The unique identifier of the sequence document.
	 */

	@Id
	private String id;

	/**
	 * The current value of the sequence.
	 */
	private long seq;

}
