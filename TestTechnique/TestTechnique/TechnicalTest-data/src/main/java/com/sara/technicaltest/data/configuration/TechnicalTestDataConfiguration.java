package com.sara.technicaltest.data.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.sara.technicaltest.data.entity.MongoUtil;
import com.sara.technicaltest.data.repository.UserRepository;

/**
 * Configuration class for the data layer
 */
@Configuration(proxyBeanMethods = false)
@EnableMongoRepositories(basePackageClasses = UserRepository.class)
public class TechnicalTestDataConfiguration {

	@Bean
	MongoUtil mongoUtil(MongoOperations mongoOperations) {
		return new MongoUtil(mongoOperations);
	}
}
