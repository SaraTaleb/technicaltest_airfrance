package com.sara.technicaltest.data.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.sara.technicaltest.data.entity.UserEntity;

/**
 * User repository
 */

//@Repository("UserRepository")
public interface UserRepository extends MongoRepository<UserEntity, Long> {


	@Query("{email:'?0'}")
	Optional<UserEntity> findByEmail(String email);
}
