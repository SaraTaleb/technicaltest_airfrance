# Technical Test


## Documentation


Please refer to the [documentation](https://gitlab.com/SaraTaleb/technicaltest_airfrance/-/blob/main/Technical%20test%20documentation.pdf?ref_type=heads) to explore the implemented web services and technologies used in this project.


## Running the Application

To start the application, run the `TechnicalTestApplication` class located in the `com.sara.technicaltest.application` package.


## Swagger

Once the application is running, access the Swagger documentation to explore endpoints and test the API:

- Swagger Documentation:[Swagger-UI](http://localhost:8080/swagger-ui/index.html#/)




This project was realized by:

sara TALEB


